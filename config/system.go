package config

type System struct {
	Host         string `mapstructure:"host" json:"host" yaml:"host"`                            // 域名
	Port         string `mapstructure:"port" json:"port" yaml:"port"`                            // 端口值
	RouterPrefix string `mapstructure:"router-prefix" json:"router-prefix" yaml:"router-prefix"` // 路由前缀
}
