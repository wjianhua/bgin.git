package consts

type UserInfo struct {
	ID    int    `json:"id"`    // 主键
	Token string `json:"token"` // token
}
