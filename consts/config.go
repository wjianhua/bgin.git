package consts

const (
	ConfigPath        = "config/"
	ConfigDefaultFile = "config.yaml"
	ConfigTestFile    = "test.yaml"
	ConfigDebugFile   = "debug.yaml"
	ConfigReleaseFile = "release.yaml"
)
