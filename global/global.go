package global

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"gorm.io/gorm"

	"github.com/redis/go-redis/v9"

	"gitee.com/wjianhua/bgin.git/config"
)

var (
	APP    *gin.Engine
	CONFIG config.Server
	VP     *viper.Viper
	DB     *gorm.DB
	Logger *zap.Logger
	REDIS  *redis.Client
)
