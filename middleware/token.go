package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitee.com/wjianhua/bgin.git/core"
	"gitee.com/wjianhua/bgin.git/core/resp"
)

func Token() gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenString := c.Request.Header.Get("token")
		if tokenString == "" {
			c.JSON(http.StatusOK, map[string]any{
				"status":  resp.RespTokenAuthFail.Code,
				"message": "缺失token",
			})
			c.Abort()
			return
		}

		res, err := core.NewToken(c).Check(tokenString)
		if err != nil || res == "" {
			c.JSON(http.StatusOK, map[string]any{
				"status":  resp.RespTokenAuthFail.Code,
				"message": "token错误",
			})
			c.Abort()
			return
		}
		c.Set("userInfo", res)
		c.Set("token", tokenString)
		c.Next()
	}
}
