package middleware

import (
	"bytes"
	"fmt"
	"gitee.com/wjianhua/bgin.git/global"
	"gitee.com/wjianhua/bgin.git/utils"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"go.uber.org/zap"
	"io"
	"strings"
	"time"
)

func RequestID() gin.HandlerFunc {
	return func(c *gin.Context) {
		timeStart := time.Now()
		requestId := c.Request.Header.Get("trace_id")
		if requestId == "" {
			u := uuid.Must(uuid.NewUUID())
			requestId = u.String()
		}

		c.Set("trace_id", requestId)
		c.Writer.Header().Set("trace_id", requestId)

		serverIP := utils.ServerIP()
		tarr := strings.Split(serverIP, ".")
		if len(tarr) > 3 {
			tarr[2] = "*"
			serverIP = strings.Join(tarr, ".")
		}
		c.Writer.Header().Set("serverIp", serverIP)

		str, _ := c.GetRawData()
		c.Request.Body = io.NopCloser(bytes.NewBuffer(str))

		c.Next()

		global.Logger.Info("["+fmt.Sprintf("%f", time.Since(timeStart).Seconds())+"] URL:["+c.Request.Method+"]"+c.Request.Host+c.Request.URL.Path+" | clientIP："+c.ClientIP(),
			zap.Any(" header:", c.Request.Header), zap.Any(" params：", c.Request.Form), zap.Any(" json：", utils.CompressStr(string(str))))
	}
}
