package bgin

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"
	"unicode"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"gitee.com/wjianhua/bgin.git/core"
	"gitee.com/wjianhua/bgin.git/global"
	"gitee.com/wjianhua/bgin.git/middleware"
)

var (
	filterRepeat map[string]bool = make(map[string]bool)
)

func init() {
	runMode := gin.DebugMode
	if os.Getenv("GIN_MODE") == "prod" {
		runMode = gin.ReleaseMode
	}
	gin.SetMode(runMode)

	global.VP = core.ReadConfig() // 初始化Viper
	global.Logger = core.NewZap() // 初始化zap日志库
	zap.ReplaceGlobals(global.Logger)

	// 初始化数据库
	if err := core.InitMysql(); err != nil {
		log.Panicf("init.Gorm err: %v", err)
	}
	// 初始化redis
	core.InitRedis()
}

func engine() {
	global.APP = gin.New()
	global.APP.Use(gin.Recovery(), middleware.RequestID())
	if gin.Mode() == gin.DebugMode {
		global.APP.Use(gin.Logger())
	}
	global.APP.NoRoute(go404)
	// 健康监测
	global.APP.GET("/health", func(c *gin.Context) {
		c.JSON(http.StatusOK, "ok")
	})
	gin.DebugPrintRouteFunc = printRouteFunc
}

func NewApp() *gin.Engine {
	if global.APP == nil {
		engine()
	}
	return global.APP
}

// 自定义debug模式路由注册打印
func printRouteFunc(httpMethod, absolutePath, handlerName string, nuHandlers int) {
	//只在开发模式打印
	if !gin.IsDebugging() {
		return
	}

	pathArr := strings.Split(absolutePath, "/")
	for k, v := range pathArr {
		pathArr[k] = lcfirst(v)
	}
	absolutePathLc := strings.Join(pathArr, "/")

	absolutePathTemp := httpMethod + "_" + strings.ToLower(absolutePath)
	if _, exist := filterRepeat[absolutePathTemp]; exist {
		return
	} else {
		filterRepeat[absolutePathTemp] = true

		format := "%-6s %-25s --> %s (%d handlers)\n"
		if !strings.HasSuffix(format, "\n") {
			format += "\n"
		}
		fmt.Fprintf(gin.DefaultWriter, "[GIN-debug] "+format, httpMethod, absolutePathLc, handlerName, nuHandlers)
	}
}

// 第一个单词首字母小写
func lcfirst(str string) string {
	for i, v := range str {
		return string(unicode.ToLower(v)) + str[i+1:]
	}
	return ""
}

// 自定义404日志
func go404(c *gin.Context) {
	c.JSON(http.StatusNotFound, map[string]interface{}{
		"status":  404,
		"message": "未找到相对应的服务",
	})
}

func Run() {
	systemConf := global.CONFIG.System
	if systemConf.Port == "" {
		global.Logger.Fatal("配置文件缺失服务端口")
		return
	}

	addr := systemConf.Host + ":" + systemConf.Port

	srv := &http.Server{
		Addr:    addr,
		Handler: global.APP,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			global.Logger.Fatal("listen: " + err.Error())
		}
	}()

	global.Logger.Info(fmt.Sprintf("服务启动成功。PID：%d，Address：%s，当前环境：%s", os.Getegid(), addr, gin.Mode()))
	// 等待中断信号以优雅地关闭服务器（设置 5 秒的超时时间）
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	global.Logger.Info("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		global.Logger.Error("Server Shutdown:" + err.Error())
	}

}
