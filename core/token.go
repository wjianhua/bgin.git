package core

import (
	"context"
	"encoding/json"
	"time"

	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"

	"gitee.com/wjianhua/bgin.git/consts"
	"gitee.com/wjianhua/bgin.git/global"
)

func NewToken(ctx context.Context) *Token {
	return &Token{
		Ctx: ctx,
	}
}

type Token struct {
	Ctx context.Context
}

func (t *Token) Get(userInfo consts.UserInfo, expiresIn time.Duration) (token string, err error) {
	token = uuid.New().String()

	userInfo.Token = token
	str, _ := json.Marshal(userInfo)
	global.REDIS.Set(t.Ctx, "token:"+token, string(str), expiresIn)
	return
}

func (t *Token) Check(token string) (res string, err error) {
	res, err = global.REDIS.Get(t.Ctx, "token:"+token).Result()
	if err == redis.Nil {
		err = nil
	}

	return
}

func (t *Token) Del(token string) (res int64, err error) {
	res, err = global.REDIS.Del(t.Ctx, "token:"+token).Result()
	if err == redis.Nil {
		err = nil
	}
	return
}
