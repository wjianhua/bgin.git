package resp

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

var (
	RespOK            = Response{Code: 0, Msg: "OK"}
	RespParamsError   = Response{Code: 1000, Msg: "参数错误"}
	RespTokenAuthFail = Response{Code: 1001, Msg: "未授权"}
	RespEXCEPTION     = Response{Code: 1003, Msg: "业务异常"}
)

type Response struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

func Result(ctx *gin.Context, code int, msg string, data interface{}) {
	// 开始时间
	ctx.JSON(http.StatusOK, Response{
		code,
		msg,
		data,
	})
}

func Ok(ctx *gin.Context) {
	Result(ctx, RespOK.Code, RespOK.Msg, RespOK.Data)
}

func OkWithMessage(ctx *gin.Context, message string) {
	Result(ctx, RespOK.Code, message, map[string]interface{}{})
}

func OkWithData(ctx *gin.Context, data interface{}) {
	Result(ctx, RespOK.Code, RespOK.Msg, data)
}

func ParamsError(ctx *gin.Context) {
	Result(ctx, RespParamsError.Code, RespParamsError.Msg, map[string]interface{}{})
}

func Fail(ctx *gin.Context) {
	Result(ctx, RespEXCEPTION.Code, RespEXCEPTION.Msg, map[string]interface{}{})
}

func FailWithMessage(ctx *gin.Context, message string) {
	Result(ctx, RespEXCEPTION.Code, message, map[string]interface{}{})
}

func FailWithDetailed(ctx *gin.Context, message string, data interface{}) {
	Result(ctx, RespEXCEPTION.Code, message, data)
}
