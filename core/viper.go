package core

import (
	"flag"
	"fmt"
	"gitee.com/wjianhua/bgin.git/consts"
	"gitee.com/wjianhua/bgin.git/global"
	"github.com/fsnotify/fsnotify"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

// ReadConfig
// 优先级: 命令行 > 环境变量 > 默认值
func ReadConfig(configPath ...string) *viper.Viper {
	var config string

	if len(configPath) == 0 {
		flag.StringVar(&config, "c", "", "choose config file.")
		flag.Parse()
		if config == "" { // 判断命令行参数是否为空
			switch gin.Mode() {
			case gin.DebugMode:
				config = consts.ConfigPath + consts.ConfigDebugFile
				fmt.Printf("您正在使用gin模式的%s环境名称,config的路径为%s\n", gin.Mode(), config)
			case gin.ReleaseMode:
				config = consts.ConfigPath + consts.ConfigReleaseFile
				fmt.Printf("您正在使用gin模式的%s环境名称,config的路径为%s\n", gin.Mode(), config)
			case gin.TestMode:
				config = consts.ConfigPath + consts.ConfigTestFile
				fmt.Printf("您正在使用gin模式的%s环境名称,config的路径为%s\n", gin.Mode(), config)
			default:
				config = consts.ConfigPath + consts.ConfigDefaultFile
				fmt.Printf("您正在使用gin模式的默认环境名称,config的路径为%s\n", config)
			}
		} else { // 命令行参数不为空 将值赋值于config
			fmt.Printf("您正在使用命令行的-c参数传递的值,config的路径为%s\n", config)
		}
	} else { // 函数传递的可变参数的第一个值赋值于config
		config = configPath[0]
		fmt.Printf("您正在使用func Viper()传递的值,config的路径为%s\n", config)
	}

	v := viper.New()
	v.SetConfigFile(config)
	v.SetConfigType("yaml")
	err := v.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	v.WatchConfig()
	v.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("config file changed:", e.Name)
		if err = v.Unmarshal(&global.CONFIG); err != nil {
			fmt.Println(err)
		}
	})

	if err = v.Unmarshal(&global.CONFIG); err != nil {
		panic(err)
	}

	return v
}
