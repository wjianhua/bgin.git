package core

import (
	"context"

	"github.com/redis/go-redis/v9"
	"go.uber.org/zap"

	"gitee.com/wjianhua/bgin.git/global"
)

func InitRedis() {
	redisCfg := global.CONFIG.Redis
	client := redis.NewClient(&redis.Options{
		Addr:     redisCfg.Addr,
		Password: redisCfg.Password, // no password set
		DB:       redisCfg.DB,       // use default DB
	})
	pong, err := client.Ping(context.Background()).Result()
	if err != nil {
		global.Logger.Error("redis connect ping failed, err:", zap.Error(err))
		panic(err)
	} else {
		global.Logger.Info("redis connect ping response:", zap.String("pong", pong))
		global.REDIS = client
	}
}
