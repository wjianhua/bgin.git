package internal

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	defaultMaxSize = 100
)

type Cutter struct {
	Filename string        `json:"filename"` // 文件名
	level    string        `json:"level"`    // 日志级别(debug, info, warn, error, dpanic, panic, fatal)
	format   string        `json:"format"`   // 时间格式
	Director string        `json:"director"` // 日志文件夹
	MaxSize  int           `json:"maxsize"`  // 最大文件大小(单位:MB)
	file     *os.File      `json:"file"`     // 文件句柄
	mu       *sync.RWMutex `json:"mu"`       // 读写锁
	size     int64         `json:"size"`     // 文件大小
}

var (
	os_Stat = os.Stat

	// byte转MB单位换算
	megabyte = 1024 * 1024
)

func NewCutter(director string, level string, maxSize int, options ...CutterOption) *Cutter {
	rotate := &Cutter{
		level:    level,
		Director: director,
		MaxSize:  maxSize,
		mu:       new(sync.RWMutex),
	}
	for i := 0; i < len(options); i++ {
		options[i](rotate)
	}

	format := time.Now().Format(rotate.format)
	formats := make([]string, 0, 4)
	formats = append(formats, rotate.Director)
	if format != "" {
		formats = append(formats, format)
	}
	formats = append(formats, rotate.level+".log")
	rotate.Filename = filepath.Join(formats...)

	return rotate
}

func (c *Cutter) Write(p []byte) (n int, err error) {
	c.mu.Lock()
	defer c.mu.Unlock()

	if c.file == nil {
		if err = c.openExistingOrNew(len(p)); err != nil {
			return 0, err
		}
	}

	writeLen := int64(len(p))
	if c.size+writeLen > c.max() {
		if err := c.rotate(); err != nil {
			return 0, err
		}
	}

	n, err = c.file.Write(p)
	c.size += int64(n)

	return n, err
}

func (c *Cutter) rotate() error {
	if err := c.close(); err != nil {
		return err
	}

	if err := c.openNew(); err != nil {
		return err
	}

	return nil
}

// Close implements io.Closer, and closes the current logfile.
func (c *Cutter) Close() error {
	c.mu.Lock()
	defer c.mu.Unlock()
	return c.close()
}

func (c *Cutter) close() error {
	if c.file == nil {
		return nil
	}
	err := c.file.Close()
	c.file = nil
	return err
}

// max returns the maximum size in bytes of log files before rolling.
func (c *Cutter) max() int64 {
	if c.MaxSize == 0 {
		return int64(defaultMaxSize * megabyte)
	}
	return int64(c.MaxSize) * int64(megabyte)
}

func (c *Cutter) openExistingOrNew(writeLen int) error {
	info, err := os_Stat(c.Filename)
	if os.IsNotExist(err) {
		return c.openNew()
	}
	if err != nil {
		return fmt.Errorf("error getting log file info: %s", err)
	}

	file, err := os.OpenFile(c.Filename, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	c.file = file
	c.size = info.Size()
	return nil
}

func (c *Cutter) openNew() error {
	err := os.MkdirAll(filepath.Dir(c.Filename), 0755)
	if err != nil {
		return fmt.Errorf("can't make directories for new logfile: %s", err)
	}

	_, err = os_Stat(c.Filename)
	if err == nil {
		c.Filename = newName(c.Filename)
	}

	f, err := os.OpenFile(c.Filename, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		return fmt.Errorf("can't open new logfile: %s", err)
	}

	c.file = f
	c.size = 0
	return nil
}

func newName(name string) string {
	dir := filepath.Dir(name)
	basename := filepath.Base(name)
	ext := filepath.Ext(basename)
	filenameOnly := strings.TrimSuffix(basename, ext)

	splits := strings.Split(filenameOnly, "_")

	var newBaseName string
	if len(splits) >= 2 {
		count, _ := strconv.Atoi(splits[len(splits)-1])
		newBaseName = fmt.Sprintf("%s_%d%s", splits[0], count+1, ext)
	} else {
		newBaseName = fmt.Sprintf("%s_1%s", filenameOnly, ext)
	}

	return filepath.Join(dir, newBaseName)
}

type CutterOption func(*Cutter)

func WithCutterFormat(format string) CutterOption {
	return func(c *Cutter) {
		c.format = format
	}
}
