package g

import (
	"context"
	"runtime/debug"

	"gitee.com/wjianhua/bgin.git/global"
)

// Go 协程创建封装
func Go(ctx context.Context, f func()) {
	go func() {
		//子协程recover，打印子协程的报错并防止因为子协程没recover错误导致的应用崩溃
		defer func() {
			if err := recover(); err != nil {
				stack := string(debug.Stack())
				global.Logger.Error("debug_stack" + stack)
			}
		}()
		f()
	}()
}
