package core

import (
	"log"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	"gitee.com/wjianhua/bgin.git/global"
)

func InitMysql() error {
	logConfig := logger.Config{
		SlowThreshold:             time.Second,   // 慢 SQL 阈值
		LogLevel:                  logger.Silent, // Log level
		IgnoreRecordNotFoundError: true,
		Colorful:                  false, // 禁用彩色打印
	}
	logConfig.LogLevel = getLogMode()
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logConfig,
	)

	// 初始化数据库
	mysqlConfig := mysql.Config{
		DSN:                       global.CONFIG.Mysql.Dsn(), // DSN data source name
		DefaultStringSize:         191,                       // string 类型字段的默认长度
		SkipInitializeWithVersion: false,                     // 根据版本自动配置
	}
	if db, err := gorm.Open(mysql.New(mysqlConfig), &gorm.Config{
		Logger: newLogger,
	}); err != nil {
		return err
	} else {
		global.DB = db
		sqlDB, _ := db.DB()
		sqlDB.SetMaxIdleConns(global.CONFIG.Mysql.MaxIdleConns)
		sqlDB.SetMaxOpenConns(global.CONFIG.Mysql.MaxOpenConns)
	}

	return nil
}

func getLogMode() (logMode logger.LogLevel) {
	switch global.CONFIG.Mysql.LogMode {
	case "silent", "Silent":
		logMode = logger.Silent
	case "error", "Error":
		logMode = logger.Error
	case "warn", "Warn":
		logMode = logger.Warn
	case "info", "Info":
		logMode = logger.Info
	default:
		logMode = logger.Info
	}

	return
}
