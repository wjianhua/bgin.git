package utils

import (
	"gitee.com/wjianhua/bgin.git/global"
	"net"
	"strings"
)

// ServerIP 服务器IP
func ServerIP() (ip string) {
	ip = "127.0.0.1"
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		global.Logger.Error("ServerIP====" + err.Error())
	}

	defer conn.Close()
	if localAddr, ok := conn.LocalAddr().(*net.UDPAddr); ok {
		ip = localAddr.IP.String()
	}

	return
}

// CompressStr 压缩字符串，去除空格或制表符
func CompressStr(str string) string {
	if str == "" {
		return ""
	}
	str = strings.Replace(str, " ", "", -1)
	str = strings.Replace(str, "\n", "", -1)
	str = strings.Replace(str, "\t", "", -1)
	return strings.Replace(str, "\r", "", -1)
}
